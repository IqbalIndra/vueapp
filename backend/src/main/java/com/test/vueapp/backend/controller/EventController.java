package com.test.vueapp.backend.controller;

import com.test.vueapp.backend.dto.EventDto;
import com.test.vueapp.backend.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/event")
public class EventController {

    @Autowired private EventService eventService;

    @GetMapping
    public List<EventDto> getAllEventDto(){
        return eventService.getAllEvents();
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public EventDto addEvent(@RequestBody EventDto eventDto){
        return eventService.addEvent(eventDto);
    }

    @GetMapping("/{id}")
    public EventDto getEventById(@PathVariable("id") Long id){
        return eventService.getEventById(id);
    }

    @PostMapping("/edit")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public EventDto editEvent(@RequestBody EventDto eventDto){
        return eventService.editEvent(eventDto);
    }
}
