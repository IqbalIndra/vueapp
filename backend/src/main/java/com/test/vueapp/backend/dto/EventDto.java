package com.test.vueapp.backend.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EventDto implements Serializable {
    private Long id;
    private String name;
    private String description;
    private Date dateEvent;
    private Date createdTime;
    private Date modifiedTime;
}
