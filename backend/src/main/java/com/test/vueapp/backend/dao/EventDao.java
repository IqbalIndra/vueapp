package com.test.vueapp.backend.dao;

import com.test.vueapp.backend.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventDao extends JpaRepository<Event , Long> {
}
