package com.test.vueapp.backend.service;

import com.test.vueapp.backend.dao.EventDao;
import com.test.vueapp.backend.dto.EventDto;
import com.test.vueapp.backend.entity.Event;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventService {
    @Autowired private EventDao eventDao;
    @Autowired private ModelMapper modelMapper;

    public List<EventDto> getAllEvents(){
        List<EventDto> eventDtos = new ArrayList<>();
        List<Event> events = eventDao.findAll();
        if(!events.isEmpty()){
            for(Event event : events){
                eventDtos.add(modelMapper.map(event, EventDto.class));
            }
        }
        return eventDtos;
    }

    public EventDto getEventById(Long id){
        Event event = eventDao.getOne(id);
        if(event != null)
            return modelMapper.map(event , EventDto.class);
        return null;
    }

    public EventDto addEvent(EventDto eventDto) {
        Event event = modelMapper.map(eventDto , Event.class);
        return modelMapper.map(eventDao.save(event) , EventDto.class);
    }

    public EventDto editEvent(EventDto eventDto) {
        Event event = modelMapper.map(eventDto , Event.class);
        return modelMapper.map(eventDao.saveAndFlush(event), EventDto.class );
    }

}
