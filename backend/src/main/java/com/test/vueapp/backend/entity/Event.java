package com.test.vueapp.backend.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NotNull
    private String name;

    @Column
    @NotNull
    private String description;

    @Column(name = "date_event")
    private Date dateEvent;
    @Column(name="created_time")
    private Date createdTime;
    @Column(name="modified_time")
    private Date modifiedTime;
}
