import EventApi from '../api/Event'

export default {
    namespaced: true,
    state: {
        isLoading : false,
        error : null,
        events: [],
        event : {}
    },
    mutations: {
        LOAD_EVENT: (state) => {
            state.isLoading = false;
            state.error = null;
            state.event = {};
        },
        ADDING_EVENT: (state) => {
            state.isLoading = true;
            state.error = null;
        },
        ADD_EVENT_SUCCESS: (state, data) => {
            state.isLoading = false;
            state.error = null;
            state.events.push(data);
        },
        ADD_EVENT_ERROR: (state, error) => {
            state.isLoading = false;
            state.error = error;
            state.event = {};
        },
        EDITING_EVENT: (state, data) => {
            state.isLoading = true;
            state.error = null;
            state.event = data;
        },
        EDIT_EVENT_SUCCESS: (state, data) => {
            state.isLoading = false;
            state.error = null;

            let idx = state.events.findIndex(event => event.id === data.id)
            state.events.splice(idx, 1, data);
        },
        EDIT_EVENT_ERROR: (state, error) => {
            state.isLoading = false;
            state.error = error;
            state.event = {};
        },
        FETCHING_EVENTS: (state) => {
            state.isLoading = true;
            state.error = null;
            state.events = [];
        },
        FETCH_EVENTS_SUCCESS: (state, data) => {
            state.isLoading = false;
            state.error = null;
            state.events = data;
        },
        FETCH_EVENTS_ERROR: (state, error) => {
            state.isLoading = false;
            state.error = error;
            state.events = [];
        },
        FETCHING_EVENT: (state) => {
            state.isLoading = true;
            state.error = null;
        },
        FETCH_EVENT_SUCCESS: (state, data) => {
            state.isLoading = false;
            state.error = null;
            state.event = data;
        },
        FETCH_EVENT_ERROR: (state, error) => {
            state.isLoading = false;
            state.error = error;
            state.event = {};
        }
    },
    getters: {
        getEvents: state => state.events,
        getEvent: state => state.event,
        isLoading : state => state.isLoading,
        hasError : state => state.error != null ,
        getError : state => state.error,
        hasEvents : state => state.events.length > 0
    },
    actions: {
        events: ({commit}) => {
            commit('FETCHING_EVENTS');
            return EventApi.listEvents()
                .then(response => commit('FETCH_EVENTS_SUCCESS',response.data))
                .catch(error => commit('FETCH_EVENTS_ERROR', error))
        },
        event: ({commit}) => {
            commit('LOAD_EVENT');
        },
        addEvent: ({dispatch , commit}, state) => {
            commit('ADDING_EVENT');
            return EventApi.addEvent(state)
                .then(response => {
                    commit('ADD_EVENT_SUCCESS',response.data);
                    setTimeout(() => {
                        dispatch('alert/success', 'Add Event successful', { root: true });
                    })
                })
                .catch(error => {
                    commit('ADD_EVENT_ERROR', error);
                    dispatch('alert/error', error, { root: true });
                })
        },
        findEventById : ({commit}, id) =>{
            commit('FETCHING_EVENT');
            return EventApi.findEventById(id)
                .then(response => {
                    commit('FETCH_EVENT_SUCCESS' , response.data);
                })
                .catch(error => {
                    commit('FETCH_EVENT_ERROR', error);
                    dispatch('alert/error', error, { root: true });
                })
        },
        editEvent: ({dispatch , commit}, state) => {
            commit('EDITING_EVENT' , state);
            return EventApi.editEvent(state)
                .then(response => {
                    commit('EDIT_EVENT_SUCCESS',response.data);
                    setTimeout(() => {
                        dispatch('alert/success', 'Edit Event successful', { root: true });
                    })
                })
                .catch(error => {
                    commit('EDIT_EVENT_ERROR', error);
                    dispatch('alert/error', error, { root: true });
                })
        }
    }
}

