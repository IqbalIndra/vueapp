import Vue from 'vue'
import VueRouter from 'vue-router'
import NProgress from 'nprogress'

import Event from '../components/__event/Event.vue'
import CreateEvent from '../components/__event/CreateEvent.vue'
import EditEvent from '../components/__event/EditEvent.vue'

Vue.use(VueRouter);

const routes = [
    {
        name : 'CreateEvent',
        path : '/event/create',
        component : CreateEvent
    },
    {
        name : 'EditEvent',
        path : '/event/edit',
        component : CreateEvent
    },
    {
        name : 'Event',
        path : '/event',
        component : Event
    }
];

export const router = new VueRouter({mode : 'history', routes : routes});

/*Router Progress Bar*/
router.beforeResolve((to, from, next)=>{
    if(to.name){
        NProgress.start()
    }
    next()
});

router.afterEach(()=>{
    NProgress.done();
});

