import axios from "axios/index";

export const AXIOS = axios.create({
    baseURL : '/api'
})