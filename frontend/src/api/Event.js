import {AXIOS} from './http-common.js'

export default {
    listEvents () {
       return AXIOS.get('/event');
    },
    addEvent(event) {
        return AXIOS.post('/event/add' , event);
    },
    findEventById(id) {
        return AXIOS.get(`/event/${id}` , id );
    },
    editEvent(event) {
        return AXIOS.post('/event/edit' , event);
    }
}