import Vue from 'vue'
import VueAxios from 'vue-axios'
import VeeValidation from 'vee-validate'
import axios from 'axios'
import VueMoment from 'vue-moment'
import $ from 'jquery'
import VueDateTimepicker  from 'vue-bootstrap-datetimepicker'
import store from './store'
import {router} from './router'

import App from './App.vue'

import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.min.css';
import '../node_modules/@fortawesome/fontawesome-free/css/fontawesome.min.css'
import '../node_modules/@fortawesome/fontawesome-free/css/solid.min.css'
import '../node_modules/@fortawesome/fontawesome-free/css/regular.min.css'
import '../node_modules/nprogress/nprogress.css'

Vue.use(VueAxios, axios);
Vue.use(VueMoment);
Vue.use(VueDateTimepicker);
Vue.use(VeeValidation);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app');

/*DateTimePicker config*/
$.extend(true, $.fn.datetimepicker.defaults, {
    icons: {
        time: 'far fa-clock',
        date: 'far fa-calendar',
        up: 'fas fa-arrow-up',
        down: 'fas fa-arrow-down',
        previous: 'fas fa-chevron-left',
        next: 'fas fa-chevron-right',
        today: 'fas fa-calendar-check',
        clear: 'far fa-trash-alt',
        close: 'far fa-times-circle'
    }
});
